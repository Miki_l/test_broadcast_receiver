package com.exmplae.testbroadcastreceiver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ILogListener {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private TextView logTextView;
    private StringBuilder stringBuilder;
    private SelfRegisteringBroadcastReceiver selfRegisteringBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logTextView = findViewById(R.id.log_text_view);
        logTextView.setMovementMethod(new ScrollingMovementMethod());
        Button createIntentButton1 = findViewById(R.id.button1);
        createIntentButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIntent1();
            }
        });

        Button createIntentButton2 = findViewById(R.id.button2);
        createIntentButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendIntent2();
            }
        });

        stringBuilder = new StringBuilder();
        selfRegisteringBroadcastReceiver = new SelfRegisteringBroadcastReceiver( this);
        registerReceiver(selfRegisteringBroadcastReceiver, SelfRegisteringBroadcastReceiver.getIntentFilter());
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(selfRegisteringBroadcastReceiver);
        super.onDestroy();
    }

    private void sendIntent1() {
        Intent intent = new Intent(SelfRegisteringBroadcastReceiver.TEST_ACTION1);
        sendBroadcast(intent);
    }

    private void sendIntent2() {
        Intent intent = new Intent(SelfRegisteringBroadcastReceiver.TEST_ACTION2);
        sendBroadcast(intent);
    }

    @Override
    public void d(String TAG, String text) {
        Log.d(TAG, text);
        stringBuilder.append("DEBUG ");
        log(TAG, text);
    }

    @Override
    public void e(String TAG, String text) {
        Log.e(TAG, text);
        stringBuilder.append("ERROR ");
        log(TAG, text);
    }

    private void log(String TAG, String text) {
        stringBuilder.append(TAG).append(" ").append(text).append(LINE_SEPARATOR);
        logTextView.setText(stringBuilder.toString());
    }

}
