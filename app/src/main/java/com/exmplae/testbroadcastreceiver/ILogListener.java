package com.exmplae.testbroadcastreceiver;

public interface ILogListener {
    void d(String TAG, String text);
    void e(String TAG, String text);
}
